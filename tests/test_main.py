from src.poney import main

def test_index():
    c = main.app.test_client()
    rv = c.get("/")
    assert b"Hello world!" == rv.data
